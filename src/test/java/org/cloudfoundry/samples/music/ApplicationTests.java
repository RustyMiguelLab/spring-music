package org.cloudfoundry.samples.music;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.cloudfoundry.samples.music.domain.RandomIdGenerator;
import static org.junit.Assert.*;	


@RunWith(SpringRunner.class)
@SpringBootTest()
public class ApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Test
    public void randomNumberTest(){
      RandomIdGenerator idGen = new RandomIdGenerator();
      String id = idGen.generateId();
      
      assertSame(id.length(), 36);	
    }

}
