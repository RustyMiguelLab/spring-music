package org.cloudfoundry.samples.music.domain;

import org.cloudfoundry.samples.music.config.CommonConfig;

public class ApplicationInfo {
    private String[] profiles;
    private String[] services;
    private String message;
    private String environment;
    private String smtpServer;
    private String smtpUser;

    public ApplicationInfo(String[] profiles, String[] services) {
        this.profiles = profiles;
        this.services = services;
    }

     public ApplicationInfo(String[] profiles, String[] services, CommonConfig config) {
        this.profiles = profiles;
        this.services = services;
        this.message = config.getMessage();
        this.environment = config.getEnvironment();
        this.smtpServer = config.getSmtpServer();
        this.smtpUser = config.getSmtpUser();
    }

    public String getMessage(){
      return this.message;
    }

    public String setMessage(String message){
      return this.message = message;
    }

    public String getEnvironment(){
      return this.environment;
    }

     public String setEnvironment(String environment){
      return this.environment = environment;
    }

    public String getSmtpServer(){
      return this.smtpServer;
    }

    public String setSmtpServer(String smtpServer){
      return this.smtpServer = smtpServer;
    }

    public String getSmtpUser(){
      return this.smtpUser;
    }

    public String setSmtpUser(String smtpUser){
      return this.smtpUser = smtpUser;
    }

    public String[] getProfiles() {
        return profiles;
    }

    public void setProfiles(String[] profiles) {
        this.profiles = profiles;
    }

    public String[] getServices() {
        return services;
    }

    public void setServices(String[] services) {
        this.services = services;
    }

}
